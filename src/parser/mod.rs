/*!
A simple parser for the `rain` intermediate representation
*/
use super::*;
use ast::*;
use elysees::Arc;
use nom::branch::*;
use nom::bytes::complete::{is_a, is_not, tag};
use nom::character::{complete::*, *};
use nom::combinator::*;
use nom::multi::*;
use nom::sequence::*;
use nom::IResult;
use num::Num;
use num_bigint::BigUint;
use std::str;
use token::*;

mod util;
pub use util::*;

/// Parse a `rain` expression
pub fn parse_expr(input: &str) -> IResult<&str, Expr> {
    alt((
        // == Basic syntax ==
        map(parse_ident, |ident| Expr::Ident(ident.into())),
        map(parse_sexpr, Expr::Sexpr),
        map(parse_tuple, Expr::Tuple),
        // == Primitives ==
        map(parse_bits, Expr::Bits),
        map(parse_natural, Expr::Natural),
        // == Functions ==
        map(parse_lambda, Expr::Lambda),
        map(parse_pi, Expr::Pi),
        map(parse_gamma, Expr::Gamma),
        map(parse_phi, Expr::Phi),
        // == Vernacular ==
        map(parse_keyword, |kw| Expr::Keyword(kw.into())),
    ))(input)
}

/// Parse an S-expression
pub fn parse_sexpr(input: &str) -> IResult<&str, Sexpr> {
    map(
        delimited(
            tag("("),
            separated_list(ws, map(parse_expr, Arc::new)),
            tag(")"),
        ),
        Sexpr,
    )(input)
}

/// Parse a tuple
pub fn parse_tuple(input: &str) -> IResult<&str, ast::Tuple> {
    map(
        delimited(
            tag("["),
            separated_list(ws, map(parse_expr, Arc::new)),
            tag("]"),
        ),
        Tuple,
    )(input)
}

/// Parse a lambda abstraction
///
/// # Examples
/// ```rust
/// # use rain_ast::parser::parse_lambda;
/// # use rain_ast::ast::{Expr, Parametrized, Lambda};
/// assert_eq!(
///     parse_lambda("\\x -> y").unwrap(),
///     ("", Lambda(Parametrized::map("x", Expr::ident("y"))))
/// );
/// assert_eq!(
///     parse_lambda("\\ a => b").unwrap(),
///     ("", Lambda(Parametrized::consume("a", Expr::ident("b"))))
/// );
/// ```
pub fn parse_lambda(input: &str) -> IResult<&str, Lambda> {
    map(
        preceded(preceded(tag(LAMBDA), opt(ws)), parse_parametrized),
        Lambda,
    )(input)
}

/// Parse a pi type
///
/// # Examples
/// ```rust
/// # use rain_ast::parser::parse_pi;
/// # use rain_ast::ast::{Expr, Parametrized, Pi};
/// assert_eq!(
///     parse_pi("#pi a -> B").unwrap(),
///     ("", Pi(Parametrized::map("a", Expr::ident("B"))))
/// );
/// assert_eq!(
///     parse_pi("#pi x => B").unwrap(),
///     ("", Pi(Parametrized::consume("x", Expr::ident("B"))))
/// );
/// ```
pub fn parse_pi(input: &str) -> IResult<&str, Pi> {
    map(preceded(preceded(tag(PI), opt(ws)), parse_parametrized), Pi)(input)
}

/// Parse a gamma node
///
/// # Examples
/// ```rust
/// # use rain_ast::parser::parse_gamma;
/// # use rain_ast::ast::{Expr, Gamma};
/// assert_eq!(
///     parse_gamma("#gamma { #true => t, #false => f }").unwrap(),
///     ("", Gamma::boolean(Expr::ident("t"), Expr::ident("f")))
/// );
/// ```
pub fn parse_gamma(input: &str) -> IResult<&str, Gamma> {
    let parse_gamma_inner = terminated(separated_list(parse_comma, parse_branch), opt(parse_comma));
    let parse_gamma_branches = delimited(
        delimited(opt(ws), tag("{"), opt(ws)),
        parse_gamma_inner,
        preceded(opt(ws), tag("}")),
    );
    map(preceded(tag(GAMMA), parse_gamma_branches), Gamma)(input)
}

fn parse_comma(input: &str) -> IResult<&str, &str> {
    delimited(opt(ws), tag(","), opt(ws))(input)
}

/// Parse a phi node
/// 
/// # Examples
/// ```rust
/// # use rain_ast::parser::parse_phi;
/// # use rain_ast::ast::{Expr, Phi, Parametrized};
/// let phi = Phi {
///     fix: vec![Parametrized::consume("f", Expr::ident("y"))],
///     yld: None,
///     rec: false
/// };
/// assert_eq!(parse_phi("#phi { f => y }").unwrap(), ("", phi));
/// ```
pub fn parse_phi(input: &str) -> IResult<&str, Phi> {
    let parse_phi_inner = separated_list(parse_comma, parse_parametrized);
    let parse_yield = preceded(preceded(tag(YIELD), opt(ws)), parse_ident);
    let parse_phi_branches = delimited(
        delimited(opt(ws), tag("{"), opt(ws)),
        pair(
            parse_phi_inner,
            opt(delimited(parse_comma, opt(parse_yield), opt(parse_comma))),
        ),
        preceded(opt(ws), tag("}")),
    );
    let parse_phi_tag = alt((map(tag(PHI), |_| false), map(tag(PHI_REC), |_| true)));
    map(
        pair(parse_phi_tag, parse_phi_branches),
        |(rec, (fix, yld))| {
            let yld = yld.flatten().map(Into::into);
            Phi { yld, fix, rec }
        },
    )(input)
}

/// Parse a parametrized expression
///
/// # Examples
/// ```rust
/// # use rain_ast::parser::parse_parametrized;
/// # use rain_ast::ast::{Expr, Parametrized, Bits};
/// assert_eq!(
///     parse_parametrized("x -> y"),
///     Ok(("", Parametrized::map("x", Expr::ident("y"))))
/// );
/// assert_eq!(
///     parse_parametrized("x => 8'h3F").unwrap(),
///     ("", Parametrized::consume("x", Expr::Bits(Bits::byte(0x3F))))
/// );
/// ```
pub fn parse_parametrized(input: &str) -> IResult<&str, Parametrized> {
    let parse_consume = alt((map(tag(MAP), |_| false), map(tag(CONSUME), |_| true)));
    map(
        tuple((parse_ident, opt(ws), parse_consume, opt(ws), parse_expr)),
        |(symbol, _, consuming, _, result)| Parametrized {
            symbol: symbol.into(),
            consuming,
            result: Arc::new(result),
        },
    )(input)
}

/// Parse a gamma node branch
///
/// # Examples
/// ```rust
/// # use rain_ast::parser::parse_branch;
/// # use rain_ast::ast::{Expr, Branch, Pattern};
/// assert_eq!(
///     parse_branch("#true => x").unwrap(),
///     ("", Branch::new(Pattern::Bool(true), Expr::ident("x")))
/// );
/// assert_eq!(
///     parse_branch("#false => y").unwrap(),
///     ("", Branch::new(Pattern::Bool(false), Expr::ident("y")))
/// );
/// ```
pub fn parse_branch(input: &str) -> IResult<&str, Branch> {
    map(
        tuple((parse_pattern, opt(ws), tag(CONSUME), opt(ws), parse_expr)),
        |(pattern, _, _, _, func)| Branch {
            pattern,
            func: Arc::new(func),
        },
    )(input)
}

/// Parse a gamma node pattern
pub fn parse_pattern(input: &str) -> IResult<&str, Pattern> {
    use Pattern::*;
    alt((
        map(tag(TRUE), |_| Bool(true)),
        map(tag(FALSE), |_| Bool(false)),
    ))(input)
}

/// The set of special characters
pub const SPECIAL_CHARACTERS: &str = "\n\r\t []()=#-<>\"\\,;";

/// Parse a `rain` keyword
///
/// A `rain` keyword is just any alphanumeric string prefixed with `#`
pub fn parse_keyword(input: &str) -> IResult<&str, &str> {
    preceded(tag("#"), alphanumeric1)(input)
}

/// Parse a `rain` identifier
///
/// A `rain` identifier may be any sequence of non-whitespace characters which does not
/// contain a special character.
///
/// # Examples
/// ```rust
/// # use rain_ast::parser::parse_ident;
/// assert_eq!(parse_ident("hello "), Ok((" ", "hello")));
/// assert!(parse_ident("0x35").is_err());
/// assert_eq!(parse_ident("x35"), Ok(("", "x35")));
/// assert_eq!(parse_ident("你好"), Ok(("", "你好")));
/// let arabic = parse_ident("الحروف العربية").unwrap();
/// let desired_arabic = (" العربية" ,"الحروف");
/// assert_eq!(arabic, desired_arabic);
/// ```
pub fn parse_ident(input: &str) -> IResult<&str, &str> {
    verify(is_not(SPECIAL_CHARACTERS), |ident: &str| {
        !is_digit(ident.as_bytes()[0])
    })(input)
}

/// Parse a `rain` bitvector display radix
///
/// A display radix may be either
/// - `h` for hexadecimal
/// - `d` for decimal
/// - `o` for octal
/// - `b` for binary
///
/// # Examples
/// ```rust
/// # use rain_ast::parser::parse_display_radix;
/// ```
pub fn parse_display_radix(input: &str) -> IResult<&str, DisplayRadix> {
    use DisplayRadix::*;
    alt((
        map(tag("h"), |_| Hex),
        map(tag("d"), |_| Dec),
        map(tag("o"), |_| Oct),
        map(tag("b"), |_| Bin),
    ))(input)
}

/// Parse a `rain` bitvector literal
///
/// `rain` supports bitvector literals written in the standard Verilog syntax, i.e.
/// `{LENGTH}'{RADIX}{DIGITS}` where `LENGTH` is the number of bits as a decimal
/// number, `RADIX` is either `h`, `d`, `o`, or `b` for hexadecimal, decimal, octal,
/// or binary and `DIGITS` is the actual digits of the number.
///
/// # Examples
/// ```rust
/// # use rain_ast::parser::parse_bits;
/// # use rain_ast::ast::Bits;
/// assert_eq!(parse_bits("8'hF4"), Ok(("", Bits::byte(0xF4))));
/// assert_eq!(parse_bits("32'd412414"), Ok(("", Bits::u32_bits(412414))));
/// ```
pub fn parse_bits(input: &str) -> IResult<&str, Bits> {
    map_res(
        tuple((parse_usize, tag("'"), parse_display_radix, hex_digit1)),
        |(len, _, radix, digits)| {
            BigUint::from_str_radix(digits, radix as u32).map(|data| Bits { len, radix, data })
        },
    )(input)
}

/// Parse a `rain` natural number literal
///
/// `rain` supports the following standard literal kinds:
/// - Decimal numbers like `456`
/// - Hexadecimal numbers like `0xcafe`
/// - Octal numbers like `0o341`
/// - Binary numbers like `0b11011`
///
/// # Examples
/// ``` rust
/// # use rain_ast::parser::parse_natural;
/// # use num::BigUint;
/// assert_eq!(parse_natural("456"), Ok(("", BigUint::from(456u32))));
/// assert_eq!(parse_natural("0xcafe"), Ok(("", BigUint::from(0xcafeu32))));
/// assert_eq!(parse_natural("0xCAFE"), Ok(("", BigUint::from(0xcafeu32))));
/// assert_eq!(parse_natural("0o341"), Ok(("", BigUint::from(0o341u32))));
/// assert_eq!(parse_natural("0b11011"), Ok(("", BigUint::from(0b11011u32))));
/// ```
pub fn parse_natural(input: &str) -> IResult<&str, BigUint> {
    map_res(
        alt((
            map(pair(tag("0x"), hex_digit1), |(_, hex)| (16, hex)),
            map(pair(tag("0o"), oct_digit1), |(_, oct)| (8, oct)),
            map(pair(tag("0b"), is_a("01")), |(_, bin)| (2, bin)),
            map(digit1, |digits| (10, digits)),
        )),
        |(rdx, digits)| BigUint::from_str_radix(digits, rdx),
    )(input)
}