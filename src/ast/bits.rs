/*!
Bitvector nodes for the `rain` AST
*/
use super::*;

/// A bitvector node
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Bits {
    /// The data backing this bitvector
    pub data: BigUint,
    /// The length of this bitvector
    pub len: usize,
    /// The display radix of this bitvector
    pub radix: DisplayRadix,
}

impl Bits {
    /// Create a bitvector of length 8 with the given data
    ///
    /// # Example
    /// ```rust
    /// # use rain_ast::ast::{Bits, DisplayRadix};
    /// let byte = Bits::byte(0x35);
    /// let desired = Bits {
    ///     data: 0x35u8.into(),
    ///     len: 8,
    ///     radix: DisplayRadix::Hex
    /// };
    /// assert_eq!(byte, desired);
    /// ```
    pub fn byte(data: u8) -> Bits {
        Bits {
            data: BigUint::from(data),
            len: 8,
            radix: DisplayRadix::Hex,
        }
    }
    /// Create a bitvector of length 16 with the given data
    ///
    /// # Example
    /// ```rust
    /// # use rain_ast::ast::{Bits, DisplayRadix};
    /// let byte = Bits::u16_bits(0xCAFE);
    /// let desired = Bits {
    ///     data: 0xCAFEu16.into(),
    ///     len: 16,
    ///     radix: DisplayRadix::Dec
    /// };
    /// assert_eq!(byte, desired);
    /// ```
    pub fn u16_bits(data: u16) -> Bits {
        Bits {
            data: BigUint::from(data),
            len: 16,
            radix: DisplayRadix::default(),
        }
    }
    /// Create a bitvector of length 32 with the given data
    ///
    /// # Example
    /// ```rust
    /// # use rain_ast::ast::{Bits, DisplayRadix};
    /// let byte = Bits::u32_bits(4000000);
    /// let desired = Bits {
    ///     data: 4000000u32.into(),
    ///     len: 32,
    ///     radix: DisplayRadix::Dec
    /// };
    /// assert_eq!(byte, desired);
    /// ```
    pub fn u32_bits(data: u32) -> Bits {
        Bits {
            data: BigUint::from(data),
            len: 32,
            radix: DisplayRadix::default(),
        }
    }
    /// Create a bitvector of length 64 with the given data
    ///  
    /// # Example
    /// ```rust
    /// # use rain_ast::ast::{Bits, DisplayRadix};
    /// let byte = Bits::u64_bits(4000000000000);
    /// let desired = Bits {
    ///     data: 4000000000000u64.into(),
    ///     len: 64,
    ///     radix: DisplayRadix::Dec
    /// };
    /// assert_eq!(byte, desired);
    /// ```
    pub fn u64_bits(data: u64) -> Bits {
        Bits {
            data: BigUint::from(data),
            len: 64,
            radix: DisplayRadix::default(),
        }
    }
}

impl Display for Bits {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        use DisplayRadix::*;
        match self.radix {
            Hex => write!(fmt, "{}'{}{:x}", HEX, self.len, self.data),
            Dec => write!(fmt, "{}'{}{}", DEC, self.len, self.data),
            Oct => write!(fmt, "{}'{}{:o}", OCT, self.len, self.data),
            Bin => write!(fmt, "{}'{}{:b}", BIN, self.len, self.data),
        }
    }
}

/// A bitvector display mode
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub enum DisplayRadix {
    /// Hexadecimal digits
    Hex = 16,
    /// Decimal digits
    Dec = 10,
    /// Octal digits
    Oct = 8,
    /// Binary digits
    Bin = 2,
}

impl Default for DisplayRadix {
    fn default() -> DisplayRadix {
        DisplayRadix::Dec
    }
}

impl Display for DisplayRadix {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        use DisplayRadix::*;
        let c = match self {
            Hex => HEX,
            Dec => DEC,
            Oct => OCT,
            Bin => BIN,
        };
        write!(fmt, "{}", c)
    }
}
