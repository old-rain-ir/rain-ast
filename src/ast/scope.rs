/*!
`rain` scopes
*/

use super::*;

/// A `rain` scope
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Scope {
    /// The statements in this scope
    pub stmts: Vec<Statement>,
    /// The result of this scope
    result: Arc<Expr>,
}

impl Display for Scope {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        write!(fmt, "{{")?;
        for stmt in self.stmts.iter() {
            write!(fmt, "\n    {}", stmt)?;
        }
        let (pre_result, post_result) = if self.stmts.len() != 0 {
            ("\n    ", "\n")
        } else {
            ("", "")
        };
        write!(fmt, "{}{}{}}}", pre_result, self.result, post_result)
    }
}

/// A statement
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub enum Statement {
    /// A `let`-statement
    Let(Let),
    /// A free parameter declaration
    Param(TypedIdent),
}

impl Display for Statement {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        use Statement::*;
        match self {
            Let(l) => Display::fmt(l, fmt),
            Param(p) => write!(fmt, "{} {};", PARAM, p),
        }
    }
}

/// A let-statement
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Let {
    /// The identifier being assigned to
    pub lhs: TypedIdent,
    /// The expression it is being assigned
    pub rhs: Arc<Expr>,
}

impl Display for Let {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        write!(fmt, "{} {} = {};", LET, self.lhs, self.rhs)
    }
}
