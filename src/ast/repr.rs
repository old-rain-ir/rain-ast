/*!
Representation nodes and types for the `rain` AST
*/
use super::*;

/// A polymorphic representation
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Forall {
    /// The argument of this polymorphic representation
    pub arg: String,
    /// The result of this polymorphic representation
    pub result: Arc<Expr>,
}

impl Display for Forall {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        write!(fmt, "{} {}, {}", FORALL, self.arg, self.result)
    }
}

/// A simple top-level function representation
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Function {
    /// The argument list of this function
    pub args: Vec<Arc<Expr>>,
    /// The result type of this function
    pub result: Arc<Expr>,
}

impl Display for Function {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        write!(fmt, "#fn [")?;
        let mut first = true;
        for arg in self.args.iter() {
            write!(fmt, "{}{}", if first { "" } else { " " }, arg)?;
            first = false;
        }
        write!(fmt, "] {}", self.result)
    }
}

/// The union of a set of representations
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Union(pub Vec<Arc<Expr>>);

impl Display for Union {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        write!(fmt, "{} {{", UNION)?;
        let mut first = true;
        for arg in self.0.iter() {
            write!(fmt, "{}{}", if first { "" } else { " " }, arg)?;
            first = false;
        }
        write!(fmt, "}}")
    }
}

/// A pointer representation
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Pointer(Arc<Expr>);

impl Display for Pointer {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        write!(fmt, "{}{}", self.0, POINTER)
    }
}

/// A primitive product representation layout
///
/// NOTE: there is an *important* difference between alignment 0 and alignment 1. Specifically,
/// values with minimum alignment >= 1 are guaranteed to be assigned a valid address, as in
/// e.g. C++, where the minimum size of a struct is 1 byte. Values with alignment 0, on the other
/// hand, may be ZSTs.
#[derive(Copy, Clone, Eq, PartialEq, Hash)]
pub enum PrimitiveLayout {
    /// Unordered, arbitrary padded layout with a given minimum alignment
    Padded(u32),
    /// Unordered, arbitrary packed layout with a given alignment (note, not minimum, just alignment!).
    /// Note this does *not* allow direct extraction of unpacked references!
    Packed(u32),
    /// Unordered, arbitrary padded layout with a given minimum alignment.
    /// Is the identity on single non-ZST components
    Transparent(u32),
    /// Ordered padded layout compatible with a platform's C compiler with a given minimum alignment
    C(u32),
}

impl Display for PrimitiveLayout {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        use PrimitiveLayout::*;
        match self {
            Padded(n) => write!(fmt, "{}({})", PADDED, n),
            Packed(n) => write!(fmt, "{}({})", PACKED, n),
            Transparent(n) => write!(fmt, "{}({})", TRANSPARENT, n),
            C(n) => write!(fmt, "{}({})", REPR_C, n),
        }
    }
}

impl Debug for PrimitiveLayout {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        Display::fmt(self, fmt)
    }
}

/// A record representation
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Record {
    /// The fields of this record
    pub fields: Vec<Arc<Expr>>,
    /// This record's layout
    pub layout: Arc<Expr>,
}

impl Display for Record {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        write!(fmt, "{}[{}] {{", RECORD, self.layout)?;
        let mut first = true;
        for field in self.fields.iter() {
            write!(fmt, "{}{}", if first { "" } else { " " }, field)?;
            first = false;
        }
        write!(fmt, "}}")
    }
}

/// A enumeration representation. Allows specially optimized sigma types, e.g. null pointer optimized enums.
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Enum {
    /// The variants of this enumeration
    pub variants: Vec<Arc<Expr>>,
    /// This enum's layout
    pub layout: Arc<Expr>,
}

impl Display for Enum {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        write!(fmt, "{}[{}] {{", ENUM, self.layout)?;
        let mut first = true;
        for variant in self.variants.iter() {
            write!(fmt, "{}{}", if first { "" } else { " " }, variant)?;
            first = false;
        }
        write!(fmt, "}}")
    }
}
