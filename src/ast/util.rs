/*!
Common fragments and utilities for `rain` AST nodes
*/
use super::*;

/// An optionally consuming parametrized value
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Parametrized {
    /// The parametrizing identifier
    pub symbol: IStr,
    /// Whether this parametrization is consuming
    pub consuming: bool,
    /// The parametrized value
    pub result: Arc<Expr>,
}

impl Parametrized {
    /// Create a new, consuming parametrized expression
    pub fn consume<S, R>(symbol: S, result: R) -> Parametrized
    where
        S: Into<IStr>,
        R: Into<Arc<Expr>>,
    {
        Parametrized {
            symbol: symbol.into(),
            result: result.into(),
            consuming: true,
        }
    }
    /// Create a new parametrized expression
    pub fn map<S, R>(symbol: S, result: R) -> Parametrized
    where
        S: Into<IStr>,
        R: Into<Arc<Expr>>,
    {
        Parametrized {
            symbol: symbol.into(),
            result: result.into(),
            consuming: false,
        }
    }
}

impl Display for Parametrized {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        let glyph = if self.consuming { CONSUME } else { MAP };
        write!(fmt, "{} {} {}", self.symbol, glyph, self.result)
    }
}

/// An optionally typed identifier
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct TypedIdent {
    /// The name of this identifier
    pub name: String,
    /// The specified type of this identifier, if any
    pub ty: Option<Arc<Expr>>,
    /// The specified representation of this identifier, if any
    pub repr: Option<Arc<Expr>>,
}

impl Display for TypedIdent {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        write!(fmt, "{}", self.name)?;
        match (&self.ty, &self.repr) {
            (Some(ty), Some(repr)) => write!(fmt, ": {} * {}", ty, repr),
            (Some(ty), None) => write!(fmt, ": {}", ty),
            (None, Some(repr)) => write!(fmt, ": _ * {}", repr),
            (None, None) => Ok(()),
        }
    }
}
