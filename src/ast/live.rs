/*!
AST nodes relating to lifetimes
*/
use super::*;

/// The meet of a set of lifetimes
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Meet(pub Vec<Arc<Expr>>);

impl Display for Meet {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        write!(fmt, "{} {{", MEET)?;
        let mut first = true;
        for arg in self.0.iter() {
            write!(fmt, "{}{}", if first { "" } else { " " }, arg)?;
            first = false;
        }
        write!(fmt, "}}")
    }
}

/// The span of time for which a given value is live and immutable
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Live(pub Arc<Expr>);

impl Display for Live {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        write!(fmt, "{}({})", LIVE, self.0)
    }
}
