/*!
Function nodes and types for the `rain` AST
*/
use super::*;

/// A lambda abstraction
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Lambda(pub Parametrized);

impl Display for Lambda {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        write!(fmt, "{}{}", LAMBDA, self.0)
    }
}

/// A pi type
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Pi(pub Parametrized);

impl Display for Pi {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        write!(fmt, "{} {}", PI, self.0)
    }
}

/// A pattern for a gamma node
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub enum Pattern {
    /// A Boolean pattern
    Bool(bool),
}

impl Display for Pattern {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        use Pattern::*;
        match self {
            Bool(b) => Display::fmt(b, fmt),
        }
    }
}

/// A branch of a gamma node
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Branch {
    /// The pattern of this branch
    pub pattern: Pattern,
    /// The function called on match
    pub func: Arc<Expr>,
}

impl Branch {
    /// Create a new branch from a pattern and function
    pub fn new<F>(pattern: Pattern, func: F) -> Branch
    where
        F: Into<Arc<Expr>>,
    {
        Branch {
            pattern,
            func: func.into(),
        }
    }
}

impl Display for Branch {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        write!(fmt, "{} {} {}", self.pattern, CONSUME, self.func)
    }
}

/// A gamma node
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Gamma(pub Vec<Branch>);

impl Gamma {
    /// Create a new Boolean gamma node with two branches
    pub fn boolean<T, F>(tt: T, ff: F) -> Gamma
    where
        T: Into<Arc<Expr>>,
        F: Into<Arc<Expr>>,
    {
        use Pattern::Bool;
        Gamma(vec![
            Branch::new(Bool(true), tt),
            Branch::new(Bool(false), ff),
        ])
    }
}

impl Display for Gamma {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        let line_sep = if self.0.len() > 2 { "\n\t" } else { " " };
        write!(fmt, "{} {{", GAMMA)?;
        let mut first = true;
        for branch in self.0.iter() {
            let branch_sep = if first { "" } else { "," };
            write!(fmt, "{}{}{}", branch_sep, line_sep, branch)?;
            first = false;
        }
        write!(fmt, "{}}}", line_sep)
    }
}

/// A phi node, representing a mutually recursive function definition fixpoint
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Phi {
    /// The typed fixpoint symbols of this phi node.
    pub fix: Vec<Parametrized>,
    /// The yield symbol of this phi node, if any
    pub yld: Option<IStr>,
    /// Whether this phi node is guaranteed to be structurally recursive and therefore terminating
    pub rec: bool,
}

impl Display for Phi {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        let len = self.fix.len() + if self.yld.is_some() { 1 } else { 0 };
        let line_sep = if len > 1 { "\n\t" } else { " " };
        write!(fmt, "{} {{", if self.rec { PHI_REC } else { PHI })?;
        let mut first = true;
        for branch in self.fix.iter() {
            let branch_sep = if first { "" } else { "," };
            write!(fmt, "{}{}{}", branch_sep, line_sep, branch)?;
            first = false;
        }
        let branch_sep = if first { "" } else { "," };
        if let Some(yld) = &self.yld {
            write!(fmt, "{}{} {}:{}", branch_sep, line_sep, YIELD, yld)?;
        }
        write!(fmt, "{}}}", line_sep)
    }
}
