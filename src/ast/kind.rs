/*!
Kinds and typing universes
*/
use super::*;

/// A typing universe
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct Universe {
    /// The sub-level of this typing universe
    pub n: u64,
    /// The level of this typing universe
    pub level: u64,
}

impl Display for Universe {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        match (self.n, self.level) {
            (0, 0) => write!(fmt, "{}", UNIVERSE),
            (n, 0) => write!(fmt, "{}({})", UNIVERSE, n),
            (n, level) => write!(fmt, "{}({}; {})", UNIVERSE, n, level),
        }
    }
}
