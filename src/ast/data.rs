/*!
`rain` nodes for data structures, e.g. tuples, sigma types, recursive data declarations, etc.
*/
use super::*;

/// A tuple of `rain` values
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Tuple(pub Vec<Arc<Expr>>);

impl Display for Tuple {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        write!(fmt, "[")?;
        let mut first = true;
        for arg in self.0.iter() {
            write!(fmt, "{}{}", if first { "" } else { " " }, arg)?;
            first = false;
        }
        write!(fmt, "]")
    }
}

/// A `rain` anonymous product type
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Product(pub Vec<Arc<Expr>>);

impl Display for Product {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        write!(fmt, "{}[", PRODUCT)?;
        let mut first = true;
        for arg in self.0.iter() {
            write!(fmt, "{}{}", if first { "" } else { " " }, arg)?;
            first = false;
        }
        write!(fmt, "]")
    }
}

/// A `rain` sigma type
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Sigma {
    /// The argument of this sigma type
    pub arg: String,
    /// The dependently typed component of this sigma type
    pub dependent: Arc<Expr>,
}

impl Display for Sigma {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        write!(fmt, "{} {} {} {}", SIGMA, self.arg, CONSUME, self.dependent)
    }
}
