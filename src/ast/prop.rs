/*!
Propositional nodes and types for the `rain` AST
*/
use super::*;

/// The identity type constructor over a given universe.
///
/// Polymorphic if `None`.
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Id(pub Option<Arc<Expr>>);

/// The reflexivity axiom over a given universe.
///
/// Polymorphic if `None`.
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Refl(pub Option<Arc<Expr>>);

/// The path induction axiom over a given universe.
///
/// Polymorphic if `None`.
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct PathInd(pub Option<Arc<Expr>>);

impl Display for Id {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        if let Some(universe) = &self.0 {
            write!(fmt, "{}({})", ID, universe)
        } else {
            write!(fmt, "{}", ID)
        }
    }
}

impl Display for Refl {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        if let Some(universe) = &self.0 {
            write!(fmt, "{}({})", REFL, universe)
        } else {
            write!(fmt, "{}", REFL)
        }
    }
}

impl Display for PathInd {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        if let Some(universe) = &self.0 {
            write!(fmt, "{}({})", PATH_IND, universe)
        } else {
            write!(fmt, "{}", PATH_IND)
        }
    }
}
