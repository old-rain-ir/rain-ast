/*!
The data structure making up the `rain` AST
*/
use super::*;
use elysees::Arc;
use internship::IStr;
use num::BigUint;
use std::fmt::{self, Debug, Display, Formatter};
use token::*;

mod bits;
mod data;
mod func;
mod kind;
mod live;
mod prop;
mod repr;
mod scope;
mod util;

pub use bits::*;
pub use data::*;
pub use func::*;
pub use kind::*;
pub use live::*;
pub use prop::*;
pub use repr::*;
pub use scope::*;
pub use util::*;

/// A `rain` expression: the core unit of the `rain` AST
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub enum Expr {
    // === Basic grammar ===
    /// A `rain` identifier
    Ident(IStr),
    /// An S-expression
    Sexpr(Sexpr),
    /// A tuple
    Tuple(Tuple),
    /// A `rain` scope
    Scope(Scope),

    // === Primitives ==
    /// A bitvector
    Bits(Bits),
    /// A natural number
    Natural(BigUint),

    // === Data structures ===
    /// A product type
    Product(Product),
    /// A sigma type
    Sigma(Sigma),

    // === Functions ===
    /// A lambda abstraction
    Lambda(Lambda),
    /// A pi type
    Pi(Pi),
    /// A gamma node
    Gamma(Gamma),
    /// A phi node
    Phi(Phi),

    // === Propositions ===
    /// The identity type constructor
    Id(Id),
    /// The reflexivity axiom
    Refl(Refl),
    /// The path induction axiom
    PathInd(PathInd),

    // === Kinds and typing universes ===
    /// A typing universe
    Universe(Universe),

    // === Representations ===
    /// A polymorphic representation
    Forall(Forall),
    /// The union of a set of representations
    Union(Union),
    /// A top-level function representation
    Function(Function),
    /// A pointer representation
    Pointer(Pointer),
    /// A record representation
    Record(Record),
    /// An enumeration representation
    Enum(Enum),

    // === Layouts ===
    /// A primitive layout
    PrimitiveLayout(PrimitiveLayout),

    // == Lifetimes ===
    /// The meet of a set of lifetimes
    Meet(Meet),
    /// The live range of a functional value
    Live(Live),

    // == Vernacular ==
    /// The type of a value.
    TypeOf(Arc<Expr>),
    /// The representation of a value
    ReprOf(Arc<Expr>),
    /// A `rain` keyword
    Keyword(IStr),
}

impl Expr {
    /// A helper function to construct an identifier expression
    pub fn ident<S>(symbol: S) -> Expr
    where
        S: Into<IStr>,
    {
        Expr::Ident(symbol.into())
    }
}

impl Display for Expr {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        use Expr::*;
        match self {
            Ident(i) => Display::fmt(i, fmt),
            Sexpr(s) => Display::fmt(s, fmt),
            Tuple(t) => Display::fmt(t, fmt),
            Scope(s) => Display::fmt(s, fmt),

            Bits(b) => Display::fmt(b, fmt),
            Natural(n) => Display::fmt(n, fmt),

            Product(p) => Display::fmt(p, fmt),
            Sigma(s) => Display::fmt(s, fmt),

            Lambda(l) => Display::fmt(l, fmt),
            Pi(p) => Display::fmt(p, fmt),
            Gamma(g) => Display::fmt(g, fmt),
            Phi(p) => Display::fmt(p, fmt),

            Id(i) => Display::fmt(i, fmt),
            Refl(r) => Display::fmt(r, fmt),
            PathInd(p) => Display::fmt(p, fmt),

            Universe(u) => Display::fmt(u, fmt),

            Forall(f) => Display::fmt(f, fmt),
            Function(f) => Display::fmt(f, fmt),
            Union(u) => Display::fmt(u, fmt),
            Pointer(p) => Display::fmt(p, fmt),
            Record(r) => Display::fmt(r, fmt),
            Enum(e) => Display::fmt(e, fmt),

            PrimitiveLayout(p) => Display::fmt(p, fmt),

            Meet(m) => Display::fmt(m, fmt),
            Live(l) => Display::fmt(l, fmt),

            TypeOf(t) => write!(fmt, "{}{}", TYPEOF, t),
            ReprOf(r) => write!(fmt, "{}{}", REPROF, r),
            Keyword(k) => write!(fmt, "#{}", k),
        }
    }
}

/// An S-expression
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Sexpr(pub Vec<Arc<Expr>>);

impl Display for Sexpr {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        write!(fmt, "(")?;
        let mut first = true;
        for arg in self.0.iter() {
            write!(fmt, "{}{}", if first { "" } else { " " }, arg)?;
            first = false;
        }
        write!(fmt, ")")
    }
}
