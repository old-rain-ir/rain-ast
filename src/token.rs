/*!
Tokens in the `rain` IR grammar
*/

// === Basic syntax tokens ===

/// The keyword for let-statements
pub const LET: &str = "#let";

/// The keyword for a free parameter declaration
pub const PARAM: &str = "#parameter";

/// The typing judgement token
pub const TY: &str = ":";

/// The type representation judgement token
pub const TYREP: &str = "*";

/// The assignment token
pub const ASSIGN: &str = "=";

// === Primitive tokens ===

/// The keyword for the boolean value `true`
pub const TRUE: &str = "#true";

/// The keyword for the boolean value `false`
pub const FALSE: &str = "#false";

/// The keyword for the type of boolean values
pub const BOOL: &str = "#bool";

/// The keyword for the type of bits
pub const BITS: &str = "#bits";

/// The radix character for hexadecimal bitvectors
pub const HEX: &str = "h";

/// The radix character for decimal bitvectors
pub const DEC: &str = "d";

/// The radix character for octal bitvectors
pub const OCT: &str = "o";

/// The radix character for binary bitvectors
pub const BIN: &str = "b";

/// The keyword for the finite type
pub const FINITE: &str = "#finite";

/// The keyword for an index into the finite type
pub const INDEX: &str = "#ix";

/// The keyword for the unit type
pub const UNIT: &str = "#unit";

/// The keyword for the empty type
pub const EMPTY: &str = "#empty";

// === Data tokens ===

/// The keyword for an array type
pub const ARRAY: &str = "#array";

/// The keyword for a product type
pub const PRODUCT: &str = "#product";

/// The keyword for a sigma type
pub const SIGMA: &str = "#sigma";

// === Function tokens ===

/// The token for a lambda abstraction
pub const LAMBDA: &str = "\\";

/// The keyword for a pi type
pub const PI: &str = "#pi";

/// The keyword for a gamma node
pub const GAMMA: &str = "#gamma";

/// The keyword for a phi node
pub const PHI: &str = "#phi";

/// The keyword for a structurally recursive phi node
pub const PHI_REC: &str = "#phi_rec";

/// The keyword for a yield symbol
pub const YIELD: &str = "#yield";

/// The token for a mapping
pub const MAP: &str = "->";

/// The token for a consuming mapping
pub const CONSUME: &str = "=>";

// === Proposition tokens ===

/// The token for the identity type
pub const ID: &str = "#id";

/// The token for the reflexivity axiom
pub const REFL: &str = "#refl";

/// The token for the path induction axiom
pub const PATH_IND: &str = "#pathind";

/// The token for the kind of propositions
pub const PROP: &str = "#prop";

// === Kind tokens ===

/// The token for a typing universe
pub const UNIVERSE: &str = "#universe";

// === Representation tokens ===

/// The keyword for a quantified representation
pub const FORALL: &str = "#forall";

/// The keyword for a union
pub const UNION: &str = "#union";

/// The keyword for a `usize`
pub const USIZE: &str = "#usize";

/// The token for a closure representation
pub const CLOSURE: &str = "#closure";

/// The token for a function representation
pub const FUNC_REPR: &str = "#fn";

/// The token for an anonymous closure environment, given a definition
pub const ANON_ENVIRONMENT: &str = "#anon_env";

/// The keyword for a simple byte-wise layout
pub const BYTES: &str = "#bytes";

/// The token for a valid (at creation time), non-null pointer
pub const POINTER: &str = "*";

/// The keyword for an array representation
pub const ARRAY_REPR: &str = "#array_repr";

/// The keyword for a record
pub const RECORD: &str = "#record";

/// The keyword for an enumeration
pub const ENUM: &str = "#enum";

/// The keyword for the null representation
pub const NULL: &str = "#null";

/// The keyword for the irrepresentable representation
pub const IRREP: &str = "#irrep";

/// The keyword for the type of representations
pub const REPR: &str = "#repr";

/// The keyword for a padded layout
pub const PADDED: &str = "#padded";

/// The keyword for a packed layout
pub const PACKED: &str = "#packed";

/// The keyword for a transparent layout
pub const TRANSPARENT: &str = "#transparent";

/// The keyword for C's layout
pub const REPR_C: &str = "#C";

/// The keyword for the type of layouts
pub const LAYOUT: &str = "#layout";

// === Lifetime tokens ===

/// The keyword for a meet of lifetimes
pub const MEET: &str = "#meet";

/// The keyword for the live range of a particular functional value
pub const LIVE: &str = "#live";

/// The keyword for the type of lifetimes
pub const LIFETIME: &str = "#lifetime";

// === Vernacular tokens ===

/// The keyword for the `typeof` operator
pub const TYPEOF: &str = "#typeof";

/// The keyword for the `reprof` operator
pub const REPROF: &str = "#reprof";
