# rain-ast
The grammar and parser for the standard textual representation of `rain` IR. 

This representation is intended both as a debugging tool and as a toy language targeting `rain` to be used as an example.
For simplicity, the IR is kept very minimal, and in particular is *not* the same as the Rust-like representation used in
some of the documentation/examples, so keep this in mind!

For more information on the `rain` project, see the [main repository](https://gitlab.com/rain-ir/rain-ir).